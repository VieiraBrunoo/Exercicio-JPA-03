package br.ucsal.bes20182.bd2.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tab_ramo_atividade")
@SequenceGenerator(name = "sq_ramo_atividade", sequenceName = "sq_ramo_atividade", allocationSize = 1)
public class RamoAtividade {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sq_ramo_atividade")
	@Column(name = "id")
	private Integer id;

	@Column(name = "nome", length = 40, nullable = false)
	private String nome;

	public RamoAtividade() {
	}

	public RamoAtividade(Integer id, String nome) {
		super();
		this.id = id;
		this.nome = nome;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RamoAtividade other = (RamoAtividade) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RamoAtividade [id=" + id + ", nome=" + nome + "]";
	}

}
