package br.ucsal.bes20182.bd2.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import br.ucsal.bes20182.bd2.converters.SituacaoVendedorConverter;
import br.ucsal.bes20182.bd2.enums.SituacaoVendedorEnum;

@Entity
@Table(name = "tab_vendedor")
// @DiscriminatorValue("V")
public class Vendedor extends Funcionario {

	@Column(name = "percentual_comissao", columnDefinition = "numeric(10,2)", nullable = false)
	private Double percentualComissao;

	@Convert(converter = SituacaoVendedorConverter.class)
	@Column(name = "situacao", columnDefinition = "char(3)", nullable = false)
	private SituacaoVendedorEnum situacao;

	@ManyToMany
	@JoinTable(name = "tab_vendedor_cliente")
	private List<PessoaJuridica> clientes;

	public Vendedor() {
	}

	public Vendedor(Double percentualComissao, SituacaoVendedorEnum situacao, List<PessoaJuridica> clientes) {
		super();
		this.percentualComissao = percentualComissao;
		this.situacao = situacao;
		this.clientes = clientes;
	}

	public Double getPercentualComissao() {
		return percentualComissao;
	}

	public void setPercentualComissao(Double percentualComissao) {
		this.percentualComissao = percentualComissao;
	}

	public SituacaoVendedorEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoVendedorEnum situacao) {
		this.situacao = situacao;
	}

	public List<PessoaJuridica> getClientes() {
		return clientes;
	}

	public void setClientes(List<PessoaJuridica> clientes) {
		this.clientes = clientes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((clientes == null) ? 0 : clientes.hashCode());
		result = prime * result + ((percentualComissao == null) ? 0 : percentualComissao.hashCode());
		result = prime * result + ((situacao == null) ? 0 : situacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vendedor other = (Vendedor) obj;
		if (clientes == null) {
			if (other.clientes != null)
				return false;
		} else if (!clientes.equals(other.clientes))
			return false;
		if (percentualComissao == null) {
			if (other.percentualComissao != null)
				return false;
		} else if (!percentualComissao.equals(other.percentualComissao))
			return false;
		if (situacao != other.situacao)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Vendedor [percentualComissao=" + percentualComissao + ", situacao=" + situacao + ", clientes="
				+ clientes + "]";
	}

 }
